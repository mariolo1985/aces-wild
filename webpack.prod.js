const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const jsEntry = {
  index: path.join(__dirname, 'src/js/index.js')
};

const cssEntry = {
  master: path.join(__dirname, 'src/less/master.less')
};

module.exports = [
  {
    name: 'Bundling JS dev',
    mode: 'production',
    entry: jsEntry,
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'js/[name].min.js'
    },
    devServer: {
      contentBase: './dist',
      hot: true,
      open: true,
      port: 3000
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          use: {
            loader: 'babel-loader'
          }
        }
      ]
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.LoaderOptionsPlugin({
        minimize: true
      }),
      new CopyWebpackPlugin([
        { from: 'src/*.html', to: path.join(__dirname, 'dist'), flatten: true }
      ])
    ]
  },
  {
    name: 'Bundling CSS dev',
    mode: 'production',
    entry: cssEntry,
    output: {
      path: path.join(__dirname, 'dist')
    },
    module: {
      rules: [
        {
          test: /\.less$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'less-loader'
          ]
        },
        {
          test: /\.(woff2?|ttf|otf|eot|svg|jpg)$/,
          exclude: /node_modules/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: '/fonts'
          }
        }
      ]
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.LoaderOptionsPlugin({
        minimize: true
      }),
      new MiniCssExtractPlugin({
        filename: 'css/master.min.css'
      })
    ]
  }
];
