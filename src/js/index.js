import React from 'react';
import { render } from 'react-dom';
import { Deck } from '../components';

(() => {
  const appElement = document.getElementById('app');
  if (appElement != null) {
    render(
      <Deck />,
      appElement
    );
  }
})();
