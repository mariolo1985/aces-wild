import React, { Component } from 'react';
import { Suit } from '.';

class Deck extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cardsDrawn: {},
      cardsRemaining: null,
      deckId: null,
      hasQueens: false
    };

    this.initDeck();
  }

  initDeck = async () => {
    const deckResponse = await fetch('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1');
    if (!deckResponse.ok) return;

    const deckInfo = await deckResponse.json();
    const { deck_id, success } = deckInfo;
    if (!success) return;

    // set deck id and call getCards once it is set
    this.setState({
      cardsDrawn: {},
      deckId: deck_id
    }, () => {
      this.getCards();
    });
  }

  hasCard = (cards, cardQuery) => {
    if ((cards['CLUBS'] === undefined)
      || (cards['DIAMONDS'] === undefined)
      || (cards['HEARTS'] === undefined)
      || (cards['SPADES'] === undefined)) return false;

    if ((cards['CLUBS'][cardQuery] === undefined)
      || (cards['DIAMONDS'][cardQuery] === undefined)
      || (cards['HEARTS'][cardQuery] === undefined)
      || (cards['SPADES'][cardQuery] === undefined)) return false;

    return true;
  }

  addWeight = (cardValue) => {
    if (cardValue === 'ACE') return 0;
    if (cardValue === 'JACK') return 11;
    if (cardValue === 'QUEEN') return 12;
    if (cardValue === 'KING') return 13;
    return null;
  }

  getCards = async () => {
    if (this.state.deckId === null) return;

    const newCardsUrl = `https://deckofcardsapi.com/api/deck/${this.state.deckId}/draw/?count=2`;
    const newCardsResponse = await fetch(newCardsUrl);

    if (!newCardsResponse.ok) return;

    const newCards = await newCardsResponse.json();
    const { cards, remaining, success } = newCards;
    if (!success) return;

    const drawnCard1 = cards[0];
    const drawnCard2 = cards[1];
    const weightedDrawnCard1 = {
      ...drawnCard1,
      weight: isNaN(drawnCard1.value) ? this.addWeight(drawnCard1.value) : drawnCard1.value
    };
    const weightedDrawnCard2 = {
      ...drawnCard2,
      weight: isNaN(drawnCard2.value) ? this.addWeight(drawnCard2.value) : drawnCard2.value
    };

    const { cardsDrawn } = this.state;
    let cardState = {
      ...cardsDrawn,
      [weightedDrawnCard1.suit]: {
        ...cardsDrawn[weightedDrawnCard1.suit],
        [weightedDrawnCard1.value]: weightedDrawnCard1
      }
    };

    cardState = {
      ...cardState,
      [weightedDrawnCard2.suit]: {
        ...cardsDrawn[weightedDrawnCard2.suit],
        [weightedDrawnCard2.value]: weightedDrawnCard2
      }
    };


    this.setState({
      cardsDrawn: cardState,
      cardsRemaining: remaining,
      hasQueens: this.hasCard(cardState, 'QUEEN')
    }, () => {
      const { cardsRemaining, hasQueens } = this.state;
      if (!hasQueens && cardsRemaining === 0) {
        this.initDeck();
        return;
      }

      if (!hasQueens) {
        setTimeout(this.getCards, 1000);
      }
    });
  }

  render() {
    const {
      cardsDrawn,
      cardsRemaining,
      deckId,
      hasQueens
    } = this.state;

    if (deckId === null) {
      return (
        <div className='cards-heading'>Retrieving a new deck</div>
      );
    }

    if (!hasQueens) {
      return (
        <div className='cards-heading'>{`Finding Queens: ${cardsRemaining === null ? 52 : cardsRemaining} cards remaining`}</div>
      );
    }

    const suits = Object.keys(cardsDrawn);
    return (
      <section className='cards-container'>
        <div className='cards-heading'>All Queens found</div>
        {
          suits.map((suit) => (
            <div className='suits-wrapper' key={suit}>
              <div className='suits-title'>{`${suit}: `}</div>
              <Suit cards={cardsDrawn} suit={suit} />
            </div>
          ))
        }
      </section>
    );
  }
}

export default Deck;
