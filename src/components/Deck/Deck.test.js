import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { configure, render, shallow } from 'enzyme';
import Deck from './Deck';

const fetch = () => {
  return Promise.resolve({
    json: () =>
      Promise.resolve(
        {
          success: true,
          shuffled: true,
          remaining: 52,
          deck_id: 'aqx7x8nuort4'
        }
      )
  });
};

global.fetch = fetch;
configure({ adapter: new Adapter() });

describe('Deck component', () => {
  let deckRenderCopy;
  beforeEach(() => {
    deckRenderCopy = render(
      <Deck />
    );
  });

  describe('Initial Render', () => {
    it('Renders correctly', () => {
      expect(deckRenderCopy.find(Deck)).toMatchSnapshot();
    });
  });

  describe('Fetching deck', () => {
    it('Fetches successfully', () => {
      const fetchSpy = jest.spyOn(window, 'fetch');
      shallow(<Deck />);

      expect(fetchSpy).toBeCalled();
      expect(fetchSpy()
        .then((response) => {
          return response.json();
        })
        .then((deckData) => {
          expect(deckData.deck_id).toEqual('aqx7x8nuort4');
        }));
    });
  });
});
