import React from 'react';
import PropTypes from 'prop-types';

const Suit = ({ cards, suit }) => {
  const cardsInSuit = cards[suit];
  const cardKeys = Object.keys(cardsInSuit);

  const cardsValueAndWeightArr = cardKeys.map((cardKey) => ({
    value: cardKey,
    weight: cardsInSuit[cardKey].weight
  }));

  const sortedCards = cardsValueAndWeightArr.sort((a, b) => {
    return a.weight - b.weight;
  });

  return (
    sortedCards.map((card) => {
      const imgSrc = cardsInSuit[card.value].image;
      return (
        <div className='suit-card-wrapper' key={`${suit}${card.value}`}>
          <img className='suit-card-img' src={imgSrc} alt={card.value} />
          <div className='suit-card-value'>{card.value}</div>
        </div>
      );
    })
  );
};

Suit.propTypes = {
  cards: PropTypes.shape({}),
  suit: PropTypes.string
};

Suit.defaultProps = {
  cards: {},
  suit: ''
};

export default Suit;
