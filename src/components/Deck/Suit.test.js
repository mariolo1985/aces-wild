import React from 'react';
import { render } from 'enzyme';
import Suit from './Suit';

describe('Suit component', () => {
  let suitRenderCopy;
  beforeEach(() => {
    const cards = {
      CLUBS: {
        QUEEN: {
          code: 'QC',
          image: 'https://deckofcardsapi.com/static/img/QC.png',
          images: {},
          suit: 'CLUBS',
          value: 'QUEEN',
          weight: 12
        }
      }
    };

    const suit = 'CLUBS';

    suitRenderCopy = render(<Suit cards={cards} suit={suit} />);
  });

  describe('Initial Render', () => {
    it('Renders correctly', () => {
      expect(suitRenderCopy.find(Suit)).toMatchSnapshot();
    });
  });
});
